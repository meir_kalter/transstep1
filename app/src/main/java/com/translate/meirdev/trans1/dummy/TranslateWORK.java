package com.translate.meirdev.trans1.dummy;

import android.content.Context;
import android.widget.Toast;

import com.translate.meirdev.trans1.MyService;
import com.translate.meirdev.trans1.gitapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

/**
 * Created by MEIRKA on 7/10/2015.
 */
public class TranslateWORK {
    private String sourceWord;
    private String twoLangualges;
    private String firstTargetWord;
    private String secondTargetWord;
    private String key = "AIzaSyBglmmS3-XPU5sNwgepqIr8KEglBezfMAE";
    private MyService reqLanguages;
    private String res1, res2;
    private String justPaste = null;


    public TranslateWORK(String sourceWord, String firstTargetWord, String secondTargetWord, Boolean twoLangs) {
        this.sourceWord = sourceWord;
        this.firstTargetWord = firstTargetWord;
        this.secondTargetWord = secondTargetWord;
        this.twoLangualges = Boolean.toString(twoLangs);
    }

    public TranslateWORK() {

    }

    public TranslateWORK translate(String translateWord) {


        String debug = "Step1;";
        showMessage("Start translation");
        System.out.println(secondTargetWord);
        String API = "https://api.github.com";
        API = "https://www.googleapis.com";
        RestAdapter restAdapter = new RestAdapter.Builder().setConverter(new StringConverter())
                .setEndpoint(API).build();
        gitapi git = restAdapter.create(gitapi.class);
        //final TranslateWORK temp = new TranslateWORK();
        reqLanguages.addWord("המתן", 1);
        if (Boolean.valueOf(twoLangualges) == false) {
            reqLanguages.addWord("המתן", 2);
        }
        reqLanguages.setTranslatedValue();
        res1 = null;
        res2 = null;
        debug = "step2";
        showMessage(debug);
        if (Boolean.valueOf(twoLangualges) == false) {

        
        git.getTranslate(key, sourceWord, secondTargetWord, translateWord, new Callback<String>() {
            @Override
            public void success(String gitmodel, Response response) {
                String debug = "step3";
                showMessage(debug);
                String str = new String("Github Name :" + gitmodel);
                int indexOf = str.indexOf("translatedText") + "translatedText".length() + 3;
                int indexOf2 = str.indexOf("\"", indexOf + 1);
                str = str.substring(indexOf + 1, indexOf2);
                str = str.replace("\"", "");
                reqLanguages.addWord(str, 2);
                res2 = str;
                debug = "step4";
                showMessage(debug);
                if (res1 != null) {
                    reqLanguages.setTranslatedValue();
                }
                debug = "step5";
                showMessage(debug);
            }

            @Override
            public void failure(RetrofitError error) {
                String str = new String(error.getMessage());

                CharSequence text = error.getMessage();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(reqLanguages.getApplicationContext(), text, duration);
                toast.show();
            }
        });
        }

        git.getTranslate(key, sourceWord, firstTargetWord, translateWord, new Callback<String>() {
            @Override
            public void success(String gitmodel, Response response) {
                String str = new String("Github Name :" + gitmodel);
                int indexOf = str.indexOf("translatedText") + "translatedText".length() + 3;
                int indexOfEnd = str.indexOf("\"", indexOf + 1);
                int indexOf2 = str.indexOf("translatedText", indexOfEnd) + "translatedText".length() + 3;
                int indexOfEnd2 = str.indexOf("\"", indexOf2 + 1);
                String str1 = str.substring(indexOf, indexOfEnd).replace("\"", "");
                String str2 = str.substring(indexOf2, indexOfEnd2).replace("\"", "");
                str = str.replace("\"", "");

                if (justPaste != null && justPaste.equals("PASTE_DATA")) {
                    reqLanguages.copyToPaste(str1);
                    return;
                }
                reqLanguages.addWord(str1, 1);
                res1 = str;

                if (Boolean.valueOf(twoLangualges) == false && res2 != null) {
                    reqLanguages.setTranslatedValue();
                } else if (Boolean.valueOf(twoLangualges) == true) {
                    reqLanguages.setTranslatedValue();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                String str = new String(error.getMessage());

                CharSequence text = error.getMessage();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(reqLanguages.getApplicationContext(), text, duration);
                toast.show();
            }
        });

        return this;
    }

    private void showMessage(String text) {
        Context context = this.getReqLanguages();

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public String getSourceWord() {
        return sourceWord;
    }

    public void setSourceWord(String sourceWord) {
        this.sourceWord = sourceWord;
    }

    public String getFirstTargetWord() {
        return firstTargetWord;
    }

    public void setFirstTargetWord(String firstTargetWord) {
        this.firstTargetWord = firstTargetWord;
    }

    public String getSecondTargetWord() {
        return secondTargetWord;
    }

    public void setSecondTargetWord(String secondTargetWord) {
        this.secondTargetWord = secondTargetWord;
    }

    public MyService getReqLanguages() {
        return reqLanguages;
    }

    public void setReqLanguages(MyService reqLanguages) {
        this.reqLanguages = reqLanguages;
    }

    public String getJustPaste() {
        return justPaste;
    }

    public void setJustPaste(String justPaste) {
        this.justPaste = justPaste;
    }


    static class StringConverter implements Converter {

        @Override
        public Object fromBody(TypedInput typedInput, Type type) throws ConversionException {
            String text = null;
            try {
                text = fromStream(typedInput.in());
            } catch (IOException ignored) {/*NOP*/ }

            return text;
        }

        @Override
        public TypedOutput toBody(Object o) {
            return null;
        }

        public static String fromStream(InputStream in) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        }
    }

    private Map<Integer, String> translatedWord = null;

}
