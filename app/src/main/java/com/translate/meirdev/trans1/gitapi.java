package com.translate.meirdev.trans1;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by MEIRKA on 7/26/2015.
 */
public interface gitapi {
    @GET("/users/{user}")
    public void getFeed(@Path("user") String user, Callback<gitmodel> response);

    @GET("/language/translate/v2")
    public void getTranslate(@Query("key") String key, @Query("source") String source, @Query("target") String target, @Query("q") String q, Callback<String> response);

    @GET("/language/translate/v2")
    public void getTranslate2(@Query("key") String key, @Query("source") String source, @Query("target") String target, @Query("q") String q, @Query("source") String source2, @Query("target") String target2, @Query("q") String q2, Callback<String> response);

}
