package com.translate.meirdev.trans1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by MEIRKA on 10/18/2015.
 */
public class transDefinition {
    public transDefinition() {
    }

    /* Inner class that defines the table contents */
    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "trans_def";
        public static final String COLUMN_NAME_ENTRY_ID = "entryid";
        public static final String COLUMN_NAME_TARGET_LINES = "target_lines";
        public static final String COLUMN_NAME_SOURCE_NAME = "source_name";
        public static final String COLUMN_NAME_ONE_LANG = "one_lang";

    }

    /**
     * Created by MEIRKA on 10/18/2015.
     */
    public static class TransPropsOpenDatabaseHelper extends OrmLiteSqliteOpenHelper {
        private static final String DATABASE_NAME = "trans_def_translation";
        private static final int DATABASE_VERSION = 1;

        /**
         * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
         */
        private Dao<TransDbProps, Long> todoDao;

        public TransPropsOpenDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION,
                    /**
                     * R.raw.ormlite_config is a reference to the ormlite_config.txt file in the
                     * /res/raw/ directory of this project
                     * */
                    R.raw.ormlite_config);
        }

        @Override
        public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
            try {

                /**
                 * creates the Todo database table
                 */
                TableUtils.createTable(connectionSource, TransDbProps.class);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
            try {
                /**
                 * Recreates the database when onUpgrade is called by the framework
                 */
                TableUtils.dropTable(connectionSource, TransDbProps.class, false);
                onCreate(sqLiteDatabase, connectionSource);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        /**
         * Returns an instance of the data access object
         *
         * @return
         * @throws SQLException
         */
        public Dao<TransDbProps, Long> getDao() throws SQLException {
            if (todoDao == null) {
                todoDao = getDao(TransDbProps.class);
            }
            return todoDao;
        }
    }


}
