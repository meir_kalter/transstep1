package com.translate.meirdev.trans1;

/**
 * Created by MEIRKA on 7/20/2015.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.translate.meirdev.trans1.dummy.ActivityStatus;
import com.translate.meirdev.trans1.dummy.ReturnValueINT;
import com.translate.meirdev.trans1.dummy.TransDefinition;
import com.translate.meirdev.trans1.dummy.TranslateWORK;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;


public class MyService extends Service implements View.OnTouchListener, ReturnValueINT {
    private ActivityStatus status;


    public TextView getTv() {
        return tv;
    }

    public Map<Integer, String> getTranslatedWord() {
        return translatedWord;
    }

    public void setTranslatedWord(Map<Integer, String> translatedWord) {
        this.translatedWord = translatedWord;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public String getTranslateText() {
        return translateText;
    }

    public void setTranslateText(String translateText) {
        this.translateText = translateText;
    }

    public String getSecTranslateLang() {
        return secTranslateLang;
    }

    public void setSecTranslateLang(String secTranslateLang) {
        this.secTranslateLang = secTranslateLang;
    }

    private String translateText;
    private String secTranslateLang;
    private Switch mySwitch;
    public void setTv(TextView tv) {
        this.tv = tv;
    }


    ClipboardManager.OnPrimaryClipChangedListener mPrimaryChangeListener = new ClipboardManager.OnPrimaryClipChangedListener() {
        @Override
        public void onPrimaryClipChanged() {
            ClipData clip = mClipboard.getPrimaryClip();

            String textForTranslate = null;
            String[] mimeTypes = clip != null ? clip.getDescription().filterMimeTypes("*/*") : null;
            if (clip.getItemAt(0).getText() != null) {
                textForTranslate = clip.getItemAt(0).getText().toString();
                showMessage("Will translate the word: " + clip.getItemAt(0).getText());
                if (serviceTransDefinition == null) {
                    showMessage("I have a problem");
                } else {
                    if (serviceTransDefinition.getOneLang() == null) {
                        try {
                            pullData();
                        } catch (SQLException e) {
                            showMessage(e.getSQLState());
                        }
                    }

                    updateClipData(textForTranslate);


                }
            }
        }
    };

    private void pullData() throws SQLException {
        transDefinition.TransPropsOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                transDefinition.TransPropsOpenDatabaseHelper.class);

        Dao<TransDbProps, Long> todoDao = todoOpenDatabaseHelper.getDao();

        List<TransDbProps> todos = todoDao.queryForAll();
        TransDbProps transDbProps = todos.get(0);
        serviceTransDefinition.setOneLang(transDbProps.getOnelang());
        this.serviceTransDefinition.setSourceName(transDbProps.getSourcename());
        this.serviceTransDefinition.setTargetLangs(new Vector());
        Scanner sc = new Scanner(transDbProps.getTargetlangs()).useDelimiter(";");
        for (int i = 0; i <= 2 && sc.hasNext(); ++i)
            this.serviceTransDefinition.getTargetLangs().add(sc.next());
    }


    void updateClipData(String reqStr) {



            TranslateWORK translateWORK = new TranslateWORK(serviceTransDefinition.getSourceName(), (String) serviceTransDefinition.getTargetLangs().get(0), (String) serviceTransDefinition.getTargetLangs().get(1), serviceTransDefinition.getOneLang().equals(Boolean.TRUE.toString()));
            translateWORK.setReqLanguages(this);
            showMessage("Fail before Step 2");
        translateWORK.translate(reqStr);

    }

    public void setTranslatedValue() {


        setTranslatedWord(translatedWord);
        updateText();


    }

    private Map<Integer, String> translatedWord = null;

    public void copyToPaste(String translatedWord) {
        mClipboard.removePrimaryClipChangedListener(mPrimaryChangeListener);
        ClipData clip = ClipData.newPlainText("Translated word", translatedWord);
        mClipboard.setPrimaryClip(clip);
        mClipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);

    }
    public void addWord(String translatedWord, int reqLangNumber) {
        if (this.translatedWord == null) {
            this.translatedWord = new HashMap<Integer, String>(2);
        }
        if (this.translatedWord.containsKey(Integer.valueOf(reqLangNumber))) {
            this.translatedWord.remove(Integer.valueOf(reqLangNumber));
        }
        this.translatedWord.put(Integer.valueOf(reqLangNumber), translatedWord);
        if (!"המתן".equals(translatedWord) && reqLangNumber == 1) {

            copyToPaste(translatedWord);

            if (bNextPaste == true) {
                Intent intentAwake = new Intent(this, ResponseActivity.class);
                intentAwake.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                bNextPaste = false;
                startActivity(intentAwake);
            }
        }

    }

    public void onDestroy() {
        showMessage("From on destroy");
    }

    private static TransDefinition serviceTransDefinition;
    ClipboardManager mClipboard;
    private TextView[] tvList = null;
    final private WindowManager.LayoutParams params = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT);
    View myview = null;


    /**
     * Called when new clients have connected to the service, after it had
     * previously been notified that all had disconnected in its
     * {@link #onUnbind}.  This will only be called if the implementation
     * of {@link #onUnbind} was overridden to return true.
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     */
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    /**
     * @param intent
     * @param startId
     * @deprecated Implement {@link #onStartCommand(Intent, int, int)} instead.
     */
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.serviceTransDefinition = new TransDefinition();
    }

    /**
     * Called by the system every time a client explicitly starts the service by calling
     * {@link Context#startService}, providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     * <p/>
     * <p>For backwards compatibility, the default implementation calls
     * {@link #onStart} and returns either {@link #START_STICKY}
     * or {@link #START_STICKY_COMPATIBILITY}.
     * <p/>
     * <p>If you need your application to run on platform versions prior to API
     * level 5, you can use the following model to handle the older {@link #onStart}
     * callback in that case.  The <code>handleCommand</code> method is implemented by
     * you as appropriate:
     * <p/>
     * {@sample development/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.java
     * start_compatibility}
     * <p/>
     * <p class="caution">Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use {@link AsyncTask}.</p>
     *
     * @param intent  The Intent supplied to {@link Context#startService},
     *                as given.  This may be null if the service is being restarted after
     *                its process has gone away, and it had previously returned anything
     *                except {@link #START_STICKY_COMPATIBILITY}.
     * @param flags   Additional data about this start request.  Currently either
     *                0, {@link #START_FLAG_REDELIVERY}, or {@link #START_FLAG_RETRY}.
     * @param startId A unique integer representing this specific request to
     *                start.  Use with {@link #stopSelfResult(int)}.
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the {@link #START_CONTINUATION_MASK} bits.
     * @see #stopSelfResult(int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.serviceTransDefinition = new TransDefinition();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Called when all clients have disconnected from a particular interface
     * published by the service.  The default implementation does nothing and
     * returns false.
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return true if you would like to have the service's
     * {@link #onRebind} method later called when new clients bind to it.
     */
    @Override
    public boolean onUnbind(Intent intent) {
        if (serviceTransDefinition == null) {
            showMessage("I have a problem");
        }
        try {

            storeTransDefDetails();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.onUnbind(intent);
    }

    private void storeTransDefDetails() throws SQLException {
        transDefinition.TransPropsOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                transDefinition.TransPropsOpenDatabaseHelper.class);

        Dao<TransDbProps, Long> todoDao = todoOpenDatabaseHelper.getDao();


        List<TransDbProps> todos = todoDao.queryForAll();
        for (int i = 0; i < todos.size(); i++)
            todoDao.delete(todos.get(i));

        String targetLangs = "";
        for (int i = 0; i < serviceTransDefinition.getTargetLangs().size(); i++) {
            targetLangs += serviceTransDefinition.getTargetLangs().get(i) + ";";
        }

        todoDao.create(new TransDbProps(1L, serviceTransDefinition.getOneLang(), serviceTransDefinition.getSourceName(), targetLangs));
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //        int left = (int) event.getX();
        int top = (int) event.getY();
        int left = (int) event.getX();
        v.layout(left, top, v.getMeasuredWidth(), v.getMeasuredHeight());


        //   if (event.getAction() == MotionEvent.ACTION_UP || Math.PI != Math.E) {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        //   windowManager.removeViewImmediate(myview);


        //     params = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
        //             WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        //|WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
        //                   ,               PixelFormat.TRANSLUCENT);

        int leftR = (int) event.getRawX();
        int topR = (int) event.getRawY();
        params.x = leftR;
        params.y = topR - result;

        //v.setY(0);
        //v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        v.bringToFront();
        // windowManager.addView(myview, params);
        windowManager.updateViewLayout(myview, params);
        //   }
        return true;

    }

    public void setStatus(ActivityStatus status) {
        // showMessage("=============Inside set status" + status);
        this.status = status;
    }

    public ActivityStatus getStatus() {
        return status;
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        showMessage(componentInfo.getClassName());
       /* List<ActivityManager.AppTask> runningTaskInfo = manager.getAppTasks();
        ActivityManager.AppTask componentInfo = runningTaskInfo.get(0);*/
        return componentInfo.getClassName().indexOf("MainActivity") > 0;
    }

    public TransDefinition getServiceTransDefinition() {

        return serviceTransDefinition;
    }

    public void setServiceTransDefinition(TransDefinition serviceTransDefinition) {
        try {

            this.serviceTransDefinition.setSourceName(serviceTransDefinition.getSourceName());
            this.serviceTransDefinition.setOneLang(serviceTransDefinition.getOneLang());
            this.serviceTransDefinition.setTargetLangs(new Vector());
            this.serviceTransDefinition.getTargetLangs().addAll(serviceTransDefinition.getTargetLangs());

        } catch (Exception e) {
            showMessage("Error while copy frommain activity");
        }


    }

    public class LocalBinder extends Binder {
        MyService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MyService.this;
        }
    }
    private final IBinder mBinder = new LocalBinder();
    private WindowManager windowManager;

    public MyService() {
    }

    @Override
    public boolean stopService(Intent name) {
        try {

            if (myview != null) windowManager.removeViewImmediate(myview);
            else
            windowManager.removeViewImmediate(getTv());
        }catch(Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "Hello toast!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        return super.stopService(name);

    }

    private NotificationReceiver nReceiver;
    private TextView tv = null;
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override public void onCreate() {
        super.onCreate();
        isForeground("");
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mClipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);
        setTv(new TextView(this));

        getTv().setText("Test");
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(670, createNotification(this));
        /*nReceiver = new NotificationReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        registerReceiver(nReceiver, filter);*/
        //     params = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
        //             WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        //|WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
        //                   ,               PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 350;
        getTv().setBackgroundColor(Color.GRAY);
        getTv().setTextColor(Color.BLUE);
        getTv().setTextSize(24);
        getTv().invalidate();
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myview = inflater.inflate(R.layout.servicedisp, null);
        tvList = new TextView[2];
        tvList[0] = (TextView) myview.findViewById(R.id.textView5);
        tvList[1] = (TextView) myview.findViewById(R.id.textViewSecLang);


        myview.setOnTouchListener(this);

        windowManager.addView(myview, params);
        registerService();

    }

    private void registerService() {
        nReceiver = new NotificationReceiver();

        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        registerReceiver(nReceiver, filter2);
    }

    public void updateText() {


        List<String> list = new ArrayList<String>(translatedWord.values());
        int index = 0;
        for (String strItem : list) {
            tvList[index].setText(strItem);
            ++index;
        }
    }

    public void changeView(int id, int numOfLang) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (myview != null) {
            myview.setOnTouchListener(null);
            windowManager.removeViewImmediate(myview);
        }
        myview = inflater.inflate(id, null);
        myview.setOnTouchListener(this);
        if (numOfLang == 2) {
            tvList = new TextView[2];
            tvList[0] = (TextView) myview.findViewById(R.id.textView5);
            tvList[1] = (TextView) myview.findViewById(R.id.textViewSecLang);

        } else {
            tvList = new TextView[1];
            tvList[0] = (TextView) myview.findViewById(R.id.textView5);


        }
        windowManager.addView(myview, params);

    }

    private Notification createNotification(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder ncBuilder = new NotificationCompat.Builder(context);
        ncBuilder.setContentTitle("My Notification");
        ncBuilder.setContentText("TranStep1 הפעלת");
        ncBuilder.setTicker("TranStep1 הפעלת");
        ncBuilder.setSmallIcon(R.drawable.icon);
        ncBuilder.setAutoCancel(false);


        Intent yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Close");
        PendingIntent pendingIntentClose = PendingIntent.getBroadcast(this, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.abc_btn_check_to_on_mtrl_015, "סגור", pendingIntentClose);


        yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Open");
        PendingIntent pendingIntentOpen = PendingIntent.getBroadcast(this, 12346, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.abc_btn_radio_to_on_mtrl_000, "פתח", pendingIntentOpen);

        yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Change languages");
        pendingIntentOpen = PendingIntent.getBroadcast(this, 12349, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.abc_btn_check_material, "שנה שפה", pendingIntentOpen);

       /* yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Add paste");
        pendingIntentOpen = PendingIntent.getBroadcast(this, 1288849, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.arrow, "Paste", pendingIntentOpen);*/


        Notification notification = ncBuilder.build();
        return notification;
    }

    public void updateUI(String messageForPaste) {
        String sourceName = (String) serviceTransDefinition.getTargetLangs().get(0);
        String toLang = serviceTransDefinition.getSourceName();
        showMessage("Applicatiion was received : " + messageForPaste);
        TranslateWORK translateWORK = new TranslateWORK(sourceName, toLang, toLang, Boolean.TRUE);
        translateWORK.setReqLanguages(this);
        translateWORK.setJustPaste("PASTE_DATA");
        showMessage("Fail before Step 2");
        translateWORK.translate(messageForPaste);
    }

    static boolean bNextPaste = false;

    public void nextTimePaste() {
        bNextPaste = true;
    }

    public void changeLanguage() {
        String sourceName = (String) serviceTransDefinition.getTargetLangs().get(0);
        String toLang = serviceTransDefinition.getSourceName();
        serviceTransDefinition.getTargetLangs().remove(0);
        serviceTransDefinition.setSourceName(sourceName);
        serviceTransDefinition.getTargetLangs().add(0, toLang);
    }
    class NotificationReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            String textForSec = intent.getExtras().getString("new text for paste");
            if (textForSec != null) {

                updateUI(textForSec);

            }
            String temp = intent.getStringExtra("Event type");
            if (temp == null)
                return;
            showMessage("Notification value========: " + temp);
            if (temp.equals("Close")) {
                hideService();

               /* if (status.equals(ActivityStatus.act_close)) {
                    showMessage("Applicatiion was closed already.: " + temp);
                    if (myview == null) {
                      //  myview.setOnTouchListener(null);
                      myview.setVisibility(View.GONE);
                   }
                }
*/
            } else if (temp.equals("Open")) {
                showService();
            } else if (temp.equals("Change languages")) {
                changeLanguage();
            } else if (temp.equals("Add paste")) {
                nextTimePaste();
            }
        }


    }

    public void showService() {
        myview.setVisibility(View.VISIBLE);
        if (serviceTransDefinition.getOneLang() == null) {
            try {
                pullData();
            } catch (SQLException e) {
                showMessage(e.getSQLState());
            }
        }

        int numOfLangs = serviceTransDefinition.getOneLang().equals(Boolean.FALSE.toString()) ? 2 : 1;
        changeView(R.layout.servicedispone, numOfLangs);
        mClipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);
    }

    public void hideService() {
        myview.setOnTouchListener(null);
        myview.setVisibility(View.GONE);
        mClipboard.removePrimaryClipChangedListener(mPrimaryChangeListener);
    }

    private void showMessage(String text) {
        Context context = this;

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        //  toast.show();
    }


    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
                showMessage("Notification value========: " + action);
            } else if (action.equals(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
                showMessage("Notification value========: " + action);
            } else if (action.equals("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE")) {
                String temp = intent.getStringExtra("Event type");
                if (temp == null) {
                    temp = "";
                }
                showMessage("Notification value========: " + action + ", [" + temp + "]");
                if (temp.equals("Open")) {
                    Intent intentAwake = new Intent(context, MainActivity.class);
                    intentAwake.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentAwake);
                } else if (temp.equals("Close")) {
                    if (myview != null) {
                        myview.setOnTouchListener(null);
                        windowManager.removeViewImmediate(myview);
                    }
                }
            }
        }
    };


}
