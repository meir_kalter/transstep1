package com.translate.meirdev.trans1;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MEIRKA on 10/18/2015.
 */
@DatabaseTable(tableName = "trans_def_translation")
public class TransDbProps {
    /**
     * Created by MEIRKA on 10/18/2015.
     */

    @DatabaseField(generatedId = true)
    private Long id;

    public TransDbProps(Long id, String onelang, String sourceName, String targetLangs) {
        this.id = id;
        this.onelang = onelang;
        this.sourcename = sourceName;
        this.targetlangs = targetLangs;
    }

    public TransDbProps() {
    }

    public String getOnelang() {
        return onelang;
    }

    public void setOnelang(String onelang) {
        this.onelang = onelang;
    }

    public String getSourcename() {
        return sourcename;
    }

    public void setSourcename(String sourcename) {
        this.sourcename = sourcename;
    }

    public String getTargetlangs() {
        return targetlangs;
    }

    public void setTargetlangs(String targetlangs) {
        this.targetlangs = targetlangs;
    }

    @DatabaseField

    private String onelang;

    @DatabaseField
    private String sourcename;

    @DatabaseField
    private String targetlangs;

}
