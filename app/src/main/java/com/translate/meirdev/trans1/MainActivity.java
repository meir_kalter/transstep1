package com.translate.meirdev.trans1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.translate.meirdev.trans1.com.translate.meirdev.utils.DBUtils;
import com.translate.meirdev.trans1.dummy.TransDefinition;
import com.translate.meirdev.trans1.dummy.TranslateWORK;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {
    Spinner spinnerOsversions, targetLang1, targetLang2;
    private TextView txtlang;
    ClipboardManager.OnPrimaryClipChangedListener mPrimaryChangeListener
            = new ClipboardManager.OnPrimaryClipChangedListener() {
        public void onPrimaryClipChanged() {
            updateClipData();
        }
    };

    View.OnKeyListener tempListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            return false;
        }
    };
    void updateClipData() {
        ClipData clip = mClipboard.getPrimaryClip();
        String[] mimeTypes = clip != null ? clip.getDescription().filterMimeTypes("*/*") : null;


        if (clip == null) {

        } else if (clip.getItemAt(0).getText() != null) {
            Intent intent = new Intent(this, MainActivity.class);
            String test6 = new String();
            test6 = clip.getItemAt(0).getText().toString();


            TranslateWORK translateWORK = new TranslateWORK(transDefinition.getSourceName(), (String) this.transDefinition.getTargetLangs().get(0), (String) transDefinition.getTargetLangs().get(1), bEnableOneLang);
            //translateWORK.setReqLanguages(this);
            showMessage("Fail before Step 2");
            translateWORK.translate(test6);

        } else if (clip.getItemAt(0).getIntent() != null) {
            Intent intent = new Intent(this, MainActivity.class);
            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            //   notificationManager.notify(0, n);
        } else if (clip.getItemAt(0).getUri() != null) {

        } else {

        }
    }
    ClipboardManager mClipboard;
    private Switch mySwitch;
    private Switch langSwitch;
    private Boolean bEnableOneLang;

    private String[] state = {"עברית", "אנגלית", "גרמנית", "רוסית", "ערבית", "צרפתית", "ספרדית", "איטלקית", "סינית", "יפנית", "טורקית", "אוקראינית", "הונגרית", "יוונית", "פורטוגזית", "קוריאנית", "הולנדית", "בנגאלית"
    };
    private String[] stateValues = {"iw", "en", "de", "ru", "ar", "fr", "es", "it", "zh", "ja", "tr", "uk", "hu", "el", "pt", "ko", "nl", "bn",
    };

    private Map<String, String> mapNames = new HashMap<String, String>();

    @SuppressWarnings("deprecation")
    private void Notify(String notificationTitle, String notificationMessage) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        @SuppressWarnings("deprecation")
        Notification notification = new Notification(R.drawable.notification_template_icon_bg,
                "New Message", System.currentTimeMillis());

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        notification.setLatestEventInfo(MainActivity.this, notificationTitle,
                notificationMessage, pendingIntent);

    }

    private TransDefinition transDefinition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            transDefinition = new TransDefinition();

            setContentView(R.layout.activity_main);
            bEnableOneLang = false;

            transDefinition.setOneLang(Boolean.FALSE.toString());
//            mClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);


            mySwitch = (Switch) findViewById(R.id.switch1);

            mySwitch.setChecked(true);
            langSwitch = (Switch) findViewById(R.id.enableOneLang);
            langSwitch.setOnCheckedChangeListener(this);

            bEnableOneLang = false;
            mySwitch.setChecked(true);
            mySwitch.setOnCheckedChangeListener(this);
            mySwitch.setChecked(false);
            mySwitch.setChecked(Math.PI != Math.E);
            spinnerOsversions = (Spinner) findViewById(R.id.osversions);
            targetLang1 = (Spinner) findViewById(R.id.targetLang);
            targetLang2 = (Spinner) findViewById(R.id.targetLang2);
            crtSpinnerLang(spinnerOsversions);
            crtSpinnerLang(targetLang1);
            crtSpinnerLang(targetLang2);


            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            int height = displaymetrics.heightPixels;
            ImageView iv = (ImageView) findViewById(R.id.imageview1);
            ViewGroup.LayoutParams params4 = iv.getLayoutParams();
            params4.width = width / 5;
            params4.height = height / 30;
            iv.setPadding(width / 22, 0, width / 22, 0);
            TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
            tl.setPadding(0, height / 10, 0, 0);
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativelayout1);
            rl.setPadding(0, 0, 0, height / 4);


            ViewGroup.LayoutParams params = spinnerOsversions.getLayoutParams();
            params.width = width / 3;
            params.height = height / 17;
            spinnerOsversions.setLayoutParams(params);

            ViewGroup.LayoutParams params2 = targetLang1.getLayoutParams();
            params2.width = width / 3;
            params2.height = height / 17;
            targetLang1.setLayoutParams(params2);
            ViewGroup.LayoutParams params3 = targetLang2.getLayoutParams();
            params3.width = width / 3;
            params3.height = height / 17;
            targetLang2.setLayoutParams(params3);

            // createNotification(this);


            Intent intent = new Intent();
            intent.setAction(MyListenerService.ACTION_NLS_CONTROL);
            intent.putExtra("command", "cancel_all");
            //    createNotification(this);
            this.sendBroadcast(intent);
            DBUtils dbUtils = new DBUtils();
            if (dbUtils.pullData(this, transDefinition)) {
                int positionSource = -1;
                for (int i = 0; positionSource < 0 && i < stateValues.length; i++) {
                    if (transDefinition.getSourceName().equals(stateValues[i])) {
                        positionSource = i;
                        spinnerOsversions.setSelection(positionSource);
                    }
                }

                int positionTarget1 = -1;
                for (int i = 0; positionTarget1 < 0 && i < stateValues.length; i++) {
                    if (transDefinition.getTargetLangs().get(0).equals(stateValues[i])) {
                        positionTarget1 = i;
                        targetLang1.setSelection(positionTarget1, true);
                    }
                }
                if (transDefinition.getTargetLangs().size() > 1) {
                    int positionTarget2 = -1;
                    for (int i = 0; positionTarget2 < 0 && i < stateValues.length; i++) {
                        if (transDefinition.getTargetLangs().get(1).equals(stateValues[i])) {
                            positionTarget2 = i;
                            targetLang2.setSelection(positionTarget2, true);
                        }
                    }
                }
            }

        } catch (Exception e) {

        }

    }

    private Notification createNotification(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder ncBuilder = new NotificationCompat.Builder(context);
        ncBuilder.setContentTitle("My Notification");
        ncBuilder.setContentText("Trans1 הפעלת");
        ncBuilder.setTicker("Trans1 הפעלת");
        ncBuilder.setSmallIcon(R.drawable.abc_btn_check_material);
        ncBuilder.setAutoCancel(false);


        Intent yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Close");
        PendingIntent pendingIntentClose = PendingIntent.getBroadcast(this, 345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.abc_btn_check_to_on_mtrl_015, "Close", pendingIntentClose);


        yesReceive = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
        yesReceive.putExtra("Event type", "Open");
        PendingIntent pendingIntentOpen = PendingIntent.getBroadcast(this, 2346, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
        ncBuilder.addAction(R.drawable.abc_btn_radio_to_on_mtrl_000, "Open", pendingIntentOpen);

        Notification notification = ncBuilder.build();
        return notification;
    }

    private NotificationReceiver nReceiver;



    private void crtSpinnerLang(Spinner spinner) {

        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> adapter_state = crtAdapter();
        spinner.setAdapter(adapter_state);
        int index = 0;
        if (mapNames.isEmpty()) {
            for (int i = 0; i < state.length; i++) {
                mapNames.put(state[index], stateValues[index]);
                ++index;
            }
        }
    }

    @NonNull
    private ArrayAdapter<String> crtAdapter() {
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                R.layout.text_spinner, state);

        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter_state;
    }

    @NonNull
    private ArrayAdapter<String> crtDummyAdapter() {
        String[] tmp = {""};
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                R.layout.text_spinner, tmp);

        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter_state;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyService.LocalBinder binder = (MyService.LocalBinder) service;
            mService = binder.getService();
            if (bEnableOneLang)
                mService.changeView(R.layout.servicedispone, 1);

            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    MyService mService;
    boolean mBound = false;


    private Map<Integer, String> translatedWord = null;

    public void addWord(String targetStr, int transItem) {
        if (translatedWord == null) {
            translatedWord = new HashMap<Integer, String>(2);
        }
        if (translatedWord.containsKey(Integer.valueOf(transItem))) {
            translatedWord.remove(Integer.valueOf(transItem));
        }
        translatedWord.put(Integer.valueOf(transItem), targetStr);


    }


    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.enableOneLang) {
            bEnableOneLang = isChecked;
            txtlang = (TextView) findViewById(R.id.textView2);
            transDefinition.setOneLang(Boolean.toString(isChecked));
            if (!isChecked) {
                txtlang.setText("2 languages");
                ArrayAdapter<String> adpter = this.crtAdapter();
                targetLang2.setAdapter(adpter);

            } else {
                txtlang.setText("1 languages");
                ArrayAdapter<String> adpter = this.crtDummyAdapter();
                targetLang2.setAdapter(adpter);
            }
            if (mService != null)
                mService.setServiceTransDefinition(transDefinition);
            targetLang2.setSelection(0);
            targetLang2.setEnabled(!isChecked);
            if (mService != null) {
                int id = R.layout.servicedisp;

                if (isChecked == true) {
                    id = R.layout.servicedispone;

                }
                mService.changeView(id, (isChecked == false) ? 2 : 1);
            }
            return;
        }
        if (isChecked) {
            if (mService == null) {
                startBackgroundService();
            } else {
                mService.showService();
            }

        } else {
            if (mService != null) {
                mService.hideService();
            }
        }

    }

    private void startBackgroundService() {
        Intent intent = new Intent(getApplicationContext(), MyService.class);

        startService(intent);
        if (!mBound) {
            bindService(intent, mConnection, BIND_AUTO_CREATE);
        }

        intent.putExtra("name", "SurvivingwithAndroid");

        getApplicationContext().startService(intent);

        //      mClipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);
        if (mService != null) {
            if (bEnableOneLang)
                mService.changeView(R.layout.servicedispone, 1);
            else
                mService.changeView(R.layout.servicedisp, 2);
        }
    }


    private String secName = "jh";

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        boolean selectedLang = true;

        switch (parent.getId()) {
            case R.id.osversions:
                spinnerOsversions.setSelection(position);
                transDefinition.setSourceName(mapNames.get(spinnerOsversions.getSelectedItem().toString()));
                if (mService != null)
                mService.setServiceTransDefinition(transDefinition);
                break;
            case R.id.targetLang:
                targetLang1.setSelection(position);
                if (!transDefinition.getTargetLangs().isEmpty()) {
                    transDefinition.getTargetLangs().remove(0);
                    String selectedStr = mapNames.get(targetLang1.getSelectedItem().toString());
                    if (bEnableOneLang) {
                        transDefinition.getTargetLangs().add(0, selectedStr);
                        transDefinition.getTargetLangs().add(1, selectedStr);
                    } else {
                        transDefinition.getTargetLangs().add(0, selectedStr);
                    }
                } else {
                    transDefinition.getTargetLangs().add(0, mapNames.get(targetLang1.getSelectedItem().toString()));
                    transDefinition.getTargetLangs().add(1, mapNames.get(targetLang1.getSelectedItem().toString()));
                }
                if (mService != null)
                    mService.setServiceTransDefinition(transDefinition);
                break;
            case R.id.targetLang2:
                targetLang2.setSelection(position);
                if (!transDefinition.getTargetLangs().isEmpty()) {
                    transDefinition.getTargetLangs().remove(1);

                    String selectedStr = mapNames.get(targetLang2.getSelectedItem().toString());
                    transDefinition.getTargetLangs().add(1, selectedStr);
                } else {
                    transDefinition.getTargetLangs().add(0, mapNames.get(targetLang2.getSelectedItem().toString()));
                    transDefinition.getTargetLangs().add(1, mapNames.get(targetLang2.getSelectedItem().toString()));
                }

                if (mService != null)
                    mService.setServiceTransDefinition(transDefinition);
                break;
            default:
                selectedLang = false;
                break;
        }

    }

    /**
     * Callback method to be invoked when the selection disappears from this
     * view. The selection can disappear for instance when touch is activated
     * or when the adapter becomes empty.
     *
     * @param parent The AdapterView that now contains no selected item.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String temp = intent.getStringExtra("Event type");
            if (temp == null)
                return;
            showMessage("Notification value: " + temp);
            if (temp.equals("Close")) {
                mySwitch.setChecked(false);
            } else if (temp.equals("Open")) {
                mySwitch.setChecked(true);
            }

        }


    }

    private EditText myEditText;

    public boolean onKey2(View view, int keyCode, KeyEvent event) {
        if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
                keyCode == EditorInfo.IME_ACTION_DONE ||
                event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

            if (!event.isShiftPressed()) {
                Log.v("AndroidEnterKeyActivity", "Enter Key Pressed!");
                switch (view.getId()) {
                    case R.id.action_bar:
                        showMessage("Just pressed the ENTER key, " +
                                "focus was on Text Box1. " +
                                "You typed:\n" + myEditText.getText());
                        break;

                }
                return true;
            }

        }
        return false; // pass on to other listeners.

    }

    private void showMessage(String text) {
        Context context = this;

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
