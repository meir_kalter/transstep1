package com.translate.meirdev.trans1.com.translate.meirdev.utils;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.translate.meirdev.trans1.TransDbProps;
import com.translate.meirdev.trans1.dummy.TransDefinition;
import com.translate.meirdev.trans1.transDefinition;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

/**
 * Created by MEIRKA on 11/8/2015.
 */
public class DBUtils {
    public void storeTransDefDetails(Context context, TransDefinition serviceTransDefinition) throws SQLException {
        transDefinition.TransPropsOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                transDefinition.TransPropsOpenDatabaseHelper.class);

        Dao<TransDbProps, Long> todoDao = todoOpenDatabaseHelper.getDao();


        List<TransDbProps> todos = todoDao.queryForAll();
        for (int i = 0; i < todos.size(); i++)
            todoDao.delete(todos.get(i));

        String targetLangs = "";
        for (int i = 0; i < serviceTransDefinition.getTargetLangs().size(); i++) {
            targetLangs += serviceTransDefinition.getTargetLangs().get(i) + ";";
        }

        todoDao.create(new TransDbProps(1L, serviceTransDefinition.getOneLang(), serviceTransDefinition.getSourceName(), targetLangs));
    }

    public boolean pullData(Context context, TransDefinition serviceTransDefinition) throws SQLException {
        transDefinition.TransPropsOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                transDefinition.TransPropsOpenDatabaseHelper.class);

        Dao<TransDbProps, Long> todoDao = todoOpenDatabaseHelper.getDao();

        List<TransDbProps> todos = todoDao.queryForAll();

        if (todos == null || todos.size() == 0) {
            return false;
        }
        TransDbProps transDbProps = todos.get(0);
        serviceTransDefinition.setOneLang(transDbProps.getOnelang());
        serviceTransDefinition.setSourceName(transDbProps.getSourcename());
        serviceTransDefinition.setTargetLangs(new Vector());
        Scanner sc = new Scanner(transDbProps.getTargetlangs()).useDelimiter(";");
        for (int i = 0; i <= 2 && sc.hasNext(); ++i)
            serviceTransDefinition.getTargetLangs().add(sc.next());
        return true;
    }
}
