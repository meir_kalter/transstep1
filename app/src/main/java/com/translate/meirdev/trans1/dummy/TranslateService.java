package com.translate.meirdev.trans1.dummy;

/**
 * Created by MEIRKA on 9/22/2015.
 */
public class TranslateService {
    private TranslateService() {

    }

    private static class SingletonHelper {
        private static final TranslateService INSTANCE = new TranslateService();
    }

    public static TranslateService getInstance() {
        return SingletonHelper.INSTANCE;
    }
}
