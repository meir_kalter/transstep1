package com.translate.meirdev.trans1.dummy;

import java.util.Vector;

/**
 * Created by MEIRKA on 10/16/2015.
 */
public class TransDefinition {
    private Vector targetLangs = new Vector(5);
    private String sourceName = "en";
    private String oneLang;
    public Vector getTargetLangs() {
        return targetLangs;
    }

    public void setTargetLangs(Vector targetLangs) {
        this.targetLangs = targetLangs;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getOneLang() {
        return oneLang;
    }

    public void setOneLang(String oneLang) {
        this.oneLang = oneLang;
    }
}
